#!/bin/bash

source environment.sh

winpty docker run -d --rm -it -p 4005:4005 -p 8080:8080 -v //host_mnt/c/home/projects/tfx:/home/lisp/quicklisp/local-projects/tfx "${DOCKER_TAG}"

# or in Visual Studio Code (with Docker extension installed) add to settings.json:
# "docker.commands.run": "docker run -d --rm -it -p 4005:4005 -p 8080:8080 -v //host_mnt/c/home/projects/tfx:/home/lisp/quicklisp/local-projects/tfx lisp:sbcl",
# "docker.commands.runInteractive": "docker run --rm -it -p 4005:4005 -p 8080:8080 -v //host_mnt/c/home/projects/tfx:/home/lisp/quicklisp/local-projects/tfx lisp:sbcl"
