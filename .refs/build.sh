#!/bin/bash

source environment.sh

docker build --build-arg SBCL_VERSION=${SBCL_VERSION} -t "${DOCKER_TAG}" .
