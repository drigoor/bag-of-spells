#!/bin/bash

SBCL_VERSION=2.1.1
export SBCL_VERSION

DOCKER_TAG=lisp-sbcl:2.1.1
export DOCKER_TAG

PROJECT_NAME=tfx
export PROJECT_NAME
